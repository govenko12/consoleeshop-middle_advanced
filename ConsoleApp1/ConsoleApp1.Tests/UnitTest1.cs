using ConsoleApp1.Consoles;
using ConsoleApp1.Menu;
using ConsoleApp1.Models;
using ConsoleApp1.servise;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;
using ConsoleApp1.Menu;
using ConsoleEShop.Models;
using ConsoleEShop.Consoles;

namespace ConsoleApp1.Tests
{
    public class UnitTest1
    {

        private List<Foods> GetSampleProducts()
        {
            return new List<Foods>
            {
                new Foods { Name = "Apple", Price = 130 },
                new Foods { Name = "tomato", Price = 120 }
            };
        }


        private List<PersonalData> GetUsers()
        {
            return new List<PersonalData>
            {
                new PersonalData { Email = "bob@ukr.net", Password = "bob",Id = 0 },
                new PersonalData { Email = "user@gmail.com", Password = "user",Id = 1 },
                new PersonalData { Email = "email0@test.com", Password = "pass" ,Id = 2}
            };

        }

        [Fact]
        public void SeeProducts_ReturnesListOfProducts()
        {
            //Arange
            Mock<Data> mokedRepository = new Mock<Data>();

            var SampleProducts = GetSampleProducts();
            mokedRepository.Setup(funk => funk.GetAllProducts()).Returns(SampleProducts);

            GuestMenu guestMenu = new GuestMenu(mokedRepository.Object, new ConsoleWrapper(), new Food_Service(mokedRepository.Object), new LoginService(mokedRepository.Object));
            string expected = SampleProducts[0].Name + "\n"
                            + SampleProducts[1].Name + "\n";

            //Act
            string actual = guestMenu.look_list_foods();

            //Assert
            Assert.Equal(expected, actual);
        }
        [Theory]
        [InlineData("Apple", 0)]
        [InlineData("tomato", 1)]
        public void FindProdByName_ValidInput_ReturnesListOfProducts(string name, int num)
        {
            //Arange
            Mock<Data> mokedRepository = new Mock<Data>();
            var SampleProducts = GetSampleProducts();

            mokedRepository.Setup(funk => funk.GetAllProducts()).Returns(SampleProducts);
            GuestMenu guestMenu = new GuestMenu(mokedRepository.Object, new ConsoleWrapper(), new Food_Service(mokedRepository.Object), new LoginService(mokedRepository.Object));

            var expected = SampleProducts[num].Name;

            //Act
            string actual = guestMenu.ProductService.find_staff_name(name);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void FindProdByName_InvalidInput_ReturnesListOfProducts()
        {
            //Arange
            Mock<Data> mokedRepository = new Mock<Data>();
            var SampleProducts = GetSampleProducts();

            mokedRepository.Setup(funk => funk.GetAllProducts()).Returns(SampleProducts);
            GuestMenu guestMenu = new GuestMenu(mokedRepository.Object, new ConsoleWrapper(), new Food_Service(mokedRepository.Object), new LoginService(mokedRepository.Object));

            var expected = "There is no such product in the store.";

            //Act
            string actual = guestMenu.ProductService.find_staff_name("lololo");

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LogIn_ValidInput_NotifiesSubscribers()
        {
            //Arange
            Mock<Data> mokedRepository = new Mock<Data>();
            var SampleUsers = GetUsers();

            bool loggedIn = false;

            mokedRepository.Setup(funk => funk.GetAllUsers()).Returns(GetUsers());
            GuestMenu guestMenu = new GuestMenu(mokedRepository.Object, new ConsoleWrapper(), new Food_Service(mokedRepository.Object), new LoginService(mokedRepository.Object));
            
            guestMenu.LoginService.NotifyOfLogginIn += (user) => loggedIn = true;

            string email = "bob@ukr.net"; string password = "bob";
            
            //Act
            guestMenu.LoginService.Log_in(email, password);

            //Assert
            Assert.True(loggedIn);

        }

        [Fact]
        public void LogIn_ValidInput_ReturnesGreeting()
        {
            //Arange
            Mock<Data> mokedRepository = new Mock<Data>();

            mokedRepository.Setup(funk => funk.GetAllUsers()).Returns(GetUsers());
            GuestMenu guestMenu = new GuestMenu(mokedRepository.Object, new ConsoleWrapper(), new Food_Service(mokedRepository.Object), new LoginService(mokedRepository.Object));

            var expected = "WellCome back!";


            string email = "bob@ukr.net"; string password = "bob";

            //Act
            string actual = guestMenu.LoginService.Log_in(email, password);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("emailWithoutAt.com", "pass", "pass","Nazar", "Email should contain '@' symbol.")]
        [InlineData("email0@test.com", "pass", "pass","Bob", "Email already registered.")]
        [InlineData("emailWith@test.com", "pass1", "pass2","Lox", "Passwords don't match!")]
        public void Register_IncorrectInput_ReturnesMessage(string email, string password, string confirmPass, string name, string message)
        {
            //Arange
            Mock<Data> mokedRepository = new Mock<Data>();

            mokedRepository.Setup(funk => funk.GetAllUsers()).Returns(GetUsers());

            GuestMenu guestMenu = new GuestMenu(mokedRepository.Object, new ConsoleWrapper(), new Food_Service(mokedRepository.Object), new LoginService(mokedRepository.Object));

            var expected = message;


           
            //Act
            string actual = guestMenu.LoginService.Registration(email, password, confirmPass, name);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Register_ValidInput_NotifiesSubscribers()
        {
            //Arange
            Mock<Data> mokedRepository = new Mock<Data>();

            bool Registered = false;
            mokedRepository.Setup(funk => funk.GetAllUsers()).Returns(GetUsers());

            mokedRepository.Setup(funk => funk.AddUser("valid@test.com", "pass","Nazar")).Returns(new PersonalData());

            GuestMenu guestMenu = new GuestMenu(mokedRepository.Object, new ConsoleWrapper(), new Food_Service(mokedRepository.Object), new LoginService(mokedRepository.Object));
            guestMenu.LoginService.NotifyOfLogginIn += (user) => Registered = true;

            //Act
            guestMenu.LoginService.Registration("valid@test.com","pass","pass","Nazar");

            //Assert
            Assert.True(Registered);
        }

        [Fact]
        public void Exit_ReturnsMessage()
        {
            //Arange
            Mock<Data> mokedRepository = new Mock<Data>();
            GuestMenu guestMenu = new GuestMenu(mokedRepository.Object, new ConsoleWrapper(), new Food_Service(mokedRepository.Object), new LoginService(mokedRepository.Object));
            var expected = "By By";

            //Act
            var actual = guestMenu.Exit();

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Exit_SetsIsActiveToFalse()
        {
            //Arange
            Mock<Data> mokedRepository = new Mock<Data>();
            GuestMenu guestMenu = new GuestMenu(mokedRepository.Object, new ConsoleWrapper(), new Food_Service(mokedRepository.Object), new LoginService(mokedRepository.Object));

            //Act
            guestMenu.Exit();

            //Assert
            Assert.False(guestMenu.IsActive);
        }


        [Theory]
        [InlineData("Apple")]
        [InlineData("tomato")]
       
        public void CreateProdByName_ValidInput_ReturnesProdName(string name)
        {
            //Arange
            Mock<Data> mokedRepository = new Mock<Data>();
            var SampleProducts = GetSampleProducts();

            mokedRepository.Setup(funk => funk.GetAllProducts()).Returns(GetSampleProducts());
            UserMenu userMenu = new UserMenu(mokedRepository.Object, new ConsoleWrapper(), new CreateOrderService(mokedRepository.Object, new List<OrderItem>()), new PersonalData());

            var expected = $"Product: '{name}', Quantity: {1}";
            

           
            
            //Act
            string actual = userMenu._createOrderService.Create_New_Order(name);

            //Assert
            Assert.Equal(expected, actual);
        }


        [Theory]
        [InlineData("Apple1")]
        [InlineData("BOB")]

        public void CreateProdByName_ValidInput_ReturnesException(string name)
        {
            //Arange
            Mock<Data> mokedRepository = new Mock<Data>();
            var SampleProducts = GetSampleProducts();

            mokedRepository.Setup(funk => funk.GetAllProducts()).Returns(GetSampleProducts());
            UserMenu userMenu = new UserMenu(mokedRepository.Object, new ConsoleWrapper(), new CreateOrderService(mokedRepository.Object, new List<OrderItem>()), new PersonalData());

            var expected = "Sorry!! No such product in the store now.";




            //Act
            string actual = userMenu._createOrderService.Create_New_Order(name);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ConfirmOrder_EmptyCart_ReturnesException()
        {
            //Arange
            Mock<CreateOrderService> createOrderServiceMock = new Mock<CreateOrderService>();


            createOrderServiceMock.Setup(funk => funk.Cart).Returns(new List<OrderItem>());
            UserMenu userMenu = new UserMenu(It.IsAny<Data>(), new ConsoleWrapper(), createOrderServiceMock.Object, It.IsAny<PersonalData>());

            var expected = "Your cart is empty.";




            //Act
            string actual = userMenu.ConfirmOrder();

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void ConfirmOrder_EmptyCart_Returnestrue()
        {
            //Arange
            Mock<CreateOrderService> createOrderServiceMock = new Mock<CreateOrderService>();
            Mock<Data> mokedRepository = new Mock<Data>();
            Mock<PersonalData> personalDataMock = new Mock<PersonalData>();
          

         // mokedRepository.Setup(funk => funk.users).Returns(GetUsers());



            personalDataMock.Setup(funk => funk.Id).Returns(1);

            createOrderServiceMock.Setup(funk => funk.Cart).Returns(new List<OrderItem>() { new OrderItem(new Foods { Name = "prod3", Price = 100 }) { Quantity = 1 } });
            UserMenu userMenu = new UserMenu(new Data(), new ConsoleWrapper(), createOrderServiceMock.Object, new PersonalData() { Email="sdassa",Password="2121",Id=1});

            var expected = "You successfuly created an order.Your Cart is Empty . Do new Order )";

            


            //Act
            string actual = userMenu.ConfirmOrder();

            //Assert
            Assert.Equal(expected, actual);
        }




        [Fact]
        public void AddProduct_AddNormal_Returnestrue()
        {
            //Arange
            Mock<Data> mokedRepository = new Mock<Data>();
            var SampleProducts = GetSampleProducts();

            mokedRepository.Setup(funk => funk.GetAllProducts()).Returns(SampleProducts);
            
            AdminService guestMenu = new AdminService(new Data());

            var expected = "Product added.";

            //Act
            string actual = guestMenu.AddProduct("bananas",122);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void AddProduct_AddPNotnormal_Returnexception()
        {
            //Arange
            Mock<Data> mokedRepository = new Mock<Data>();
            var SampleProducts = GetSampleProducts();

            mokedRepository.Setup(funk => funk.GetAllProducts()).Returns(SampleProducts);

            AdminService guestMenu = new AdminService(new Data());

            var expected = "Invalid price!";

            //Act
            string actual = guestMenu.AddProduct("bananas", -1);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ModifyProduct_Check_id_Returntrue()
        {
            //Arange
         
            AdminService guestMenu = new AdminService(new Data());

            var expected = "No product with such ID";

            //Act
            string actual = guestMenu.ModifyProduct(222, "test", 3);

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void ModifyProduct_invalid_price_Returntrue()
        {
            //Arange

            AdminService guestMenu = new AdminService(new Data());

            var expected = "Invalid price!";

            //Act
            string actual = guestMenu.ModifyProduct(1, "test", -1);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ModifyProduct_Okay_all_Returntrue()
        {
            //Arange

            AdminService guestMenu = new AdminService(new Data());

            var expected = "Product changed.";

            //Act
            string actual = guestMenu.ModifyProduct(1, "test", 10);

            //Assert
            Assert.Equal(expected, actual);
        }


    }
}
