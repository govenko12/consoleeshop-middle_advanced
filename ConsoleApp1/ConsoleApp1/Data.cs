﻿using ConsoleApp1.Models;
using ConsoleEShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp1
{
    public class Data: IRepository
    {



        public static List<Foods> products = new List<Foods>()
        {
            new Foods { Name = "Apple", Type = "fruts",Description="" ,Price = 25},
            new Foods { Name = "Melon", Type = "berry" ,Description="",Price = 18},
            new Foods { Name = "tomato", Type = "vegatable",Description="",Price = 56},
            new Foods { Name = "banana", Type = "fruts",Description="",Price = 29},
            new Foods { Name = "cherry", Type = "berry",Description="",Price = 50}

        };

        public  static List<PersonalData> users { get; set; } = new List<PersonalData>()
        {
            new PersonalData { Email = "admin", Password = "admin", IsAdmin = true },


            new PersonalData { Email = "bob", Password = "bob" },
            new PersonalData { Email = "user@gmail.com", Password = "user" }
        };




        private static List<Order> orders = new List<Order>()
        {
         

        };
        public virtual Foods GetProductByName(string name)
        {
            return products.FirstOrDefault(prod => prod.Name == name);
        }



        public virtual List<Foods> GetAllProducts()
        {
            return products;
        }

        public virtual List<PersonalData> GetAllUsers()
        {
            return users;
        }

        public virtual PersonalData AddUser(string email, string password,string name)
        {
            if (!email.Contains("@"))
                throw new ArgumentException("Email should contain '@' symbol", nameof(email));
            if (String.IsNullOrWhiteSpace(password))
                throw new ArgumentException("Password shouldn't be white space or empty", nameof(password));
            if (name == "")
                throw new ArgumentException("Enter Name", nameof(name));

            PersonalData newUser = new PersonalData()
            {
                Password = password,
                Email = email,
                Name = name

            };



            users.Add(newUser);

            return newUser;
        }


        public void AddOrder(Order order)
        {
            if (!(users.Any(user => user.Id == order.UserId)))
                throw new ArgumentException("user Id not found", nameof(order));
            if (order.Items.Count == 0)
                throw new ArgumentException("items list is empty", nameof(order));

            orders.Add(order);
        }


        public Order GetOrderById(int orderId)
        {
            return orders.FirstOrDefault(ord => ord.Id == orderId);
        }

        public List<Order> GetOrdersByUserId(int userId)
        {
            return orders.Where(ord => ord.UserId == userId).ToList();
        }


        public bool UpdateProduct(int prodId, Foods product)
        {
            if (String.IsNullOrWhiteSpace(product.Name))
                throw new ArgumentException("Name shouldn't be white space or empty", nameof(product));
            if (product.Price <= 0)
                throw new ArgumentException("Price should be greater than 0", nameof(product));

            Foods targetProduct = products.FirstOrDefault(prod => prod.Id == prodId);

            if (!ProductNameAvailable(product.Name) && (product.Name != targetProduct.Name))
                return false;

            if (targetProduct != null/* && (ProductNameAvailable(product.Name) || (product.Name == targetProduct.Name))*/)
            {
                targetProduct.Name = product.Name;
                targetProduct.Price = product.Price;
                return true;
            }
            return false;
        }

        public bool UpdateUser(PersonalData user)
        {
            if (!user.Email.Contains("@"))
                throw new ArgumentException("Email should contain '@' symbol", nameof(user));
            if (String.IsNullOrWhiteSpace(user.Password))
                throw new ArgumentException("Password shouldn't be white space or empty", nameof(user));

            PersonalData targetUser = users.FirstOrDefault(u => u.Id == user.Id);

            if (targetUser != null)
            {
                targetUser.Email = user.Email;
                targetUser.Password = user.Password;
                return true;
            }
            return false;
        }

        private bool ProductNameAvailable(string name)
        {
            var names = GetAllProducts().Select(p => p.Name);
            if (names.Contains(name) || String.IsNullOrWhiteSpace(name))
                return false;
            return true;
        }


        public PersonalData GetUserById(int userId)
        {
            return users.FirstOrDefault(user => user.Id == userId);
        }

        public PersonalData GetUserByEmail(string email)
        {
            return users.FirstOrDefault(user => user.Email == email);
        }


        public bool AddProduct(Foods product)
        {
            if (String.IsNullOrWhiteSpace(product.Name))
                throw new ArgumentException("Name shouldn't be white space or empty", nameof(product));
            if (product.Price <= 0)
                throw new ArgumentException("Price should be greater than 0", nameof(product));

            if (ProductNameAvailable(product.Name) && product.Price > 0)
            {
                products.Add(product);
                return true;
            }
            return false;
        }

        public Foods GetProductById(int prodId)
        {
            return products.FirstOrDefault(prod => prod.Id == prodId);
        }

        public bool SetNewOrderStatus(int orderId, OrderStatus status)
        {
            Order targetOrder = orders.FirstOrDefault(ord => ord.Id == orderId);

            if (targetOrder != null)
            {
                targetOrder.status = status;
                return true;
            }
            return false;
        }

        public  PersonalData AddUser(string email, string password)
        {
            if (!email.Contains("@"))
                throw new ArgumentException("Email should contain '@' symbol", nameof(email));
            if (String.IsNullOrWhiteSpace(password))
                throw new ArgumentException("Password shouldn't be white space or empty", nameof(password));

            PersonalData newUser = new PersonalData()
            {
                Password = password,
                Email = email
            };

            users.Add(newUser);

            return newUser;
        }
    }
}
