﻿using ConsoleApp1.Models;
using ConsoleApp1.servise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp1.Menu
{
  public   class GuestMenu :IMenu , ILook_find_staff, IregisteredUser
    {

        public bool IsActive { get; set; } = true;
        private readonly Data _data;

        // public delegate void LogInHendler(PersonalData user);
        //  public event LogInHendler NotifyOfLogginIn;

        public Food_Service ProductService { get; set; }
        public LoginService LoginService { get; set; }

        public GuestMenu(Data data, Food_Service service,LoginService log)
        {
            _data = data;
            ProductService = service;
            LoginService = log;
        }



       

        

        public string find_staff_by_name()
        {
            Console.WriteLine("Input name of the product:");

            string name = Console.ReadLine().Trim();

            return ProductService.find_staff_name(name);
        }




        public string ChooseOptions()
        {
            PrintOptions();

            return ExecuteOption(GetOption());
        }

        private string ExecuteOption(string opt)
        {
            Dictionary<string, Func<string>> dict = GetOptionDict();

            try
            {
                return dict[opt]();
            }
            catch (KeyNotFoundException)
            {
                return "Incorrect input!";
            }
        }

        private Dictionary<string, Func<string>> GetOptionDict()
        {
            var dict = new Dictionary<string, Func<string>>();

            dict.Add("1", look_list_foods);

            dict.Add("2", find_staff_by_name);
            
            dict.Add("3", Log_in);

            dict.Add("4", Registration);
            dict.Add("5", Exit);

            return dict;
        }


        private string GetOption()
        {
            return Console.ReadLine().Trim();
        }




        public string look_list_foods()
        {
            string res = String.Empty;

            foreach (var item in _data.GetAllProducts())
            {
                res += item.Name + "\n";
            }

            return res;


        }

        private void PrintOptions()
        {
            Console.WriteLine("1) See products");
            Console.WriteLine("2) Find product by name");
            Console.WriteLine("3) Log In");
            Console.WriteLine("4) Register");
            Console.WriteLine("5) Exit");
        }

        public string Log_in()
        {
            Console.WriteLine("Email:");
            string email = Console.ReadLine();
            Console.WriteLine("Password:");
            string password = Console.ReadLine();

            
           return LoginService.Log_in(email,password);



           
        }

        public string Registration()
        {
            Console.WriteLine("Email:");
            string email = Console.ReadLine();

           
            

            Console.WriteLine("Password:");
            string password = Console.ReadLine();

            Console.WriteLine("Confirm password:");
            string confPass = Console.ReadLine();

           

            Console.WriteLine("Name:");
            string name = Console.ReadLine();




            return LoginService.Registration(email, password, confPass, name);
        }

        

        public string Exit()
        {
            IsActive = false;
            return "By By"; 
        }
    }
}
