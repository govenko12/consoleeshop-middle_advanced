﻿using ConsoleApp1.Models;
using ConsoleApp1.servise;
using ConsoleEShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp1.Menu
{
    public class UserMenu : IMenu, ILook_find_staff,IDO_order
    {
        public PersonalData User { get; }

        public bool IsActive { get; set; } = true;
       

        public delegate void LogOutHendler();
        public event LogOutHendler NotifyOfLoggingOut;

        private protected List<OrderItem> Cart { get; set; }
        public bool Get_status { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        protected readonly Data _data;

        public CreateOrderService _createOrderService { get; set; }

        private protected readonly IRepository _repo;
        public UserMenu(Data data,CreateOrderService createOrderService ,PersonalData user)
        {
            _data = data;
            _createOrderService = createOrderService;
            User = user;
            Cart = _createOrderService.Cart;

        }


        public string ChooseOptions()
        {
            
                PrintOptions();

                return ExecuteOption(GetOption());
            

        }
        private string ExecuteOption(string opt)
        {
            Dictionary<string, Func<string>> dict = GetOptionDict();

            try
            {
                return dict[opt]();
            }
            catch (KeyNotFoundException)
            {
                return "Incorrect input!";
            }
        }

        protected virtual Dictionary<string, Func<string>> GetOptionDict()
        {
            var dict = new Dictionary<string, Func<string>>();

            dict.Add("1", look_list_foods);
            dict.Add("2", find_staff_by_name);
            dict.Add("3", Create_New_Order);
            dict.Add("4", ShowCart);
            dict.Add("5", ConfirmOrder);
            dict.Add("6", ClearCart);
            dict.Add("7", SeeOrderHistory);
            dict.Add("8", SetStatusReceived);
            dict.Add("9", CancelOrder);
            dict.Add("10", ChangePassword);
            dict.Add("11", LogOut);
            dict.Add("12", Exit);

            return dict;
        }



        private string GetOption()
        {
            return Console.ReadLine().Trim();
        }

        

       

        public string look_list_foods()
        {
            string res = String.Empty;

            foreach (var item in _data.GetAllProducts())
            {
                res += item.Name + "\n";
            }

            return res;


        }
      

        public string find_staff_by_name()
        {
            Console.WriteLine("Input name of the product:");

            string name = Console.ReadLine().Trim();

            Foods find_foods = _data.GetAllProducts().FirstOrDefault(prod => prod.Name == name);




            return find_foods.Name + " " + find_foods.Price;

        }



        protected virtual void PrintOptions()
        {
            Console.WriteLine("1) See products");
            Console.WriteLine("2) Find product by name");
            Console.WriteLine("3) Add product to cart");
            Console.WriteLine("4) Show cart");
            Console.WriteLine("5) Confirm order");
            Console.WriteLine("6) Clear cart");
            Console.WriteLine("7) See order history");
            Console.WriteLine("8) Set 'Received' status");
            Console.WriteLine("9) Cancel order");
            Console.WriteLine("10) Change password");
            Console.WriteLine("11) Log Out");
            Console.WriteLine("12) Вихід");
        }

        public string Create_New_Order()
        {
            Console.WriteLine("Input the name of the product:");
            string name = Console.ReadLine();

           
            return _createOrderService.Create_New_Order(name);
        }

        
        
        public string ShowCart()
        {
            if (Cart.Count == 0)
                Console.WriteLine("DO Order(3).Cart is Empty Now! ");
            return GetStringOfEnumerable<OrderItem>(Cart);
        }
      
        private protected string GetStringOfEnumerable<T>(IEnumerable<T> enumer)
        {
            string res = String.Empty;

            foreach (T item in enumer)
            {
                res += item.ToString() + "\n";
            }

            return res;
        }

        public string ConfirmOrder()
        {
            if (Cart.Count == 0)
            {
                return "Your cart is empty.";
            }

            Order order = new Order()
            {
                Items = Cart,
                UserId = User.Id
            };

          

            _data.AddOrder(order);
            
            Cart = new List<OrderItem>();
            return "You successfuly created an order.Your Cart is Empty . Do new Order )";
                
        }
         public string ClearCart()
         {
            Cart = new List<OrderItem>();
            return "Your cart is empty.";
         }
        public string SeeOrderHistory()
        {
            List<Order> userOrders = _data.GetOrdersByUserId(User.Id);

            return GetStringOfEnumerable<Order>(userOrders);
        }
        public string SetStatusReceived()
        {
            Console.WriteLine("Input ID of order you received:");
            try
            {
                int Id = Convert.ToInt32(Console.ReadLine());
                if (!_data.GetOrdersByUserId(User.Id).Select(o => o.Id).Contains(Id))
                {
                    return "There is no order in your history with such ID.";
                }
                _data.GetOrderById(Id).status = OrderStatus.Received;
                return $"Status 'Received' set to order with ID {Id}";
            }
            catch (OverflowException)
            {
                return "outside the range of the Int32 type.";
            }
            catch (FormatException)
            {
                return "Not valid Id!";
            }
        }
        
         public string CancelOrder()
        {
            Console.WriteLine("Input ID of order you want to cancel:");
            try
            {
                int Id = Convert.ToInt32(Console.ReadLine());
                if (!_data.GetOrdersByUserId(User.Id).Select(o => o.Id).Contains(Id))
                {
                    return "There is no order in your history with such ID.";
                }
                var order = _data.GetOrderById(Id);

                if (order.status == OrderStatus.Received ||
                    order.status == OrderStatus.Complete ||
                    order.status == OrderStatus.CanceledByUser ||
                    order.status == OrderStatus.CanceledByAdmin)
                {
                    return "You can't cancel this order";
                }

                order.status = OrderStatus.CanceledByUser;
               
                return $"Order with ID {Id} cancelled.";
            }
            catch (OverflowException)
            {
                return "outside the range of the Int32 type.";
            }
            catch (FormatException)
            {
                return "Not valid Id!";
            }
        }



        public string ChangePassword()
        {
            Console.WriteLine("Input your old password:");
            string pass = Console.ReadLine();

            if (pass != User.Password)
            {
                return "Wrong password";
            }

            Console.WriteLine("Input your new password:");
            string newPass = Console.ReadLine();

            User.Password = newPass;
            return "Password changed.";
        }

        public string LogOut()
        {
            NotifyOfLoggingOut?.Invoke();
            return "Logging Out...";
        }

        public string Exit()
        {
            IsActive = false;
            return "By by";
        }



       

        
    }
}
