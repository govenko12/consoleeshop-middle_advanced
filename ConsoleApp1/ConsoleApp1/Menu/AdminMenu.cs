﻿using ConsoleApp1.Models;
using ConsoleApp1.servise;
using ConsoleEShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp1.Menu
{
   public class AdminMenu :UserMenu
    {


        public AdminService adminService { get; set; }
        public AdminMenu(Data _data, CreateOrderService createOrderService, PersonalData user,AdminService ad) : base(_data, createOrderService, user)
        {

            adminService = ad;
        }

        



       

        public string SeeUsersInfo()
        {
            List<PersonalData> users = _data.GetAllUsers();

            return GetStringOfEnumerable<PersonalData>(users);
        }



        

        public string ChangeUserPassword()
        {
            Console.WriteLine("Input the ID of user you want to modify:");

            int userId;
            //TODO: Create method InputId() and simplify this method
            try
            {
                userId = Convert.ToInt32(Console.ReadLine());
            }
            catch (OverflowException)
            {
                return "outside the range of the Int32 type.";
            }
            catch (FormatException)
            {
                return "Not valid Id!";
            }

            PersonalData user = _data.GetUserById(userId);
            if (user is null)
            {
                return "No user thith such ID";
            }

            Console.WriteLine("New password:");
            var newPassword = Console.ReadLine();
            if (String.IsNullOrWhiteSpace(newPassword))
            {
                return "Invalid password!";
            }
            user.Password = newPassword;

            _data.UpdateUser(user);

            return "Password changed.";
        }

        public string AddProduct()
        {
            Console.WriteLine("Name:");
            string name = Console.ReadLine();

            Console.WriteLine("Price:");
            int price = Convert.ToInt32(Console.ReadLine());

            return adminService.AddProduct(name, price);

        }


        public string ModifyProduct()
        {
            Console.WriteLine("Input ID of the product:");
            int prodId = Convert.ToInt32(Console.ReadLine());

            Foods prod = _data.GetProductById(prodId);

            if (prod is null)
            {
                return "No product with such ID";
            }

            Foods newProd = new Foods();

            Console.WriteLine("New name:");
            newProd.Name = Console.ReadLine();

            Console.WriteLine("New price:");
            newProd.Price = Convert.ToInt32(Console.ReadLine());
            if (newProd.Price <= 0)
            {
                return "Invalid price!";
            }

            bool success = _data.UpdateProduct(prodId, newProd);
            if (success)
            {
                return "Product changed.";
            }
            return "Invaliv product data";
        }

        public string SeeOrdersOfUser()
        {
            Console.WriteLine("Input ID of the user:");
            int userId = Convert.ToInt32(Console.ReadLine());

            if (!UserExists(userId))
            {
                return "No user with such Id!";
            }

            List<Order> orders = _data.GetOrdersByUserId(userId);

            if (orders.Count == 0)
            {
                return "User hasn't made any purchase yet.";
            }

            return GetStringOfEnumerable<Order>(orders);
        }

        private bool UserExists(int userId)
        {
            var userIds = _data.GetAllUsers().Select(u => u.Id);
            if (userIds.Contains(userId))
                return true;
            return false;
        }

        public string ChangeOrderStatus()
        {
            Console.WriteLine("Input ID of the order:");
            int orderId = Convert.ToInt32(Console.ReadLine());

            Order order = _data.GetOrderById(orderId);
            if (order is null)
            {
                return "No order with such ID!";
            }

            Console.WriteLine("Select status you want this order to have(New, Canceled, PaymentReceived, Sent, Complete,):");
            OrderStatus status;
            switch (Console.ReadLine().ToLower())
            {
                case "new":
                    status = OrderStatus.New;
                    break;
                case "canceled":
                    status = OrderStatus.CanceledByAdmin;
                    break;
                case "paymentreceived":
                    status = OrderStatus.PaymentReceived;
                    break;
                case "sent":
                    status = OrderStatus.Sent;
                    break;
                case "complete":
                    status = OrderStatus.Complete;
                    break;
                default:
                    return "Invalid status!";
            }
            _data.SetNewOrderStatus(orderId, status);
            return "Status changed.";
        }

        protected override Dictionary<string, Func<string>> GetOptionDict()
        {
            var dict = new Dictionary<string, Func<string>>();

            dict.Add("1", SeeUsersInfo);
            dict.Add("2", ChangeUserPassword);
            dict.Add("3", look_list_foods);

            dict.Add("4", find_staff_by_name);
            dict.Add("5", Create_New_Order);
            dict.Add("6", ShowCart);
            dict.Add("7", ConfirmOrder);
            dict.Add("8", ClearCart);

            dict.Add("9", AddProduct);
          
           
            dict.Add("10", ChangeOrderStatus);
            dict.Add("11", LogOut);
            dict.Add("12", Exit);

            return dict;
        }

        protected override void PrintOptions()
        {
            Console.WriteLine("1) See users info");
            Console.WriteLine("2) Change user's password");
            Console.WriteLine("3) See products");

            Console.WriteLine("4) Find product by name");
            Console.WriteLine("5) Add product to cart");
           Console.WriteLine("6) Show cart");
            Console.WriteLine("7) Confirm order");
            Console.WriteLine("8) Empty cart");


            Console.WriteLine("9) Add product");
     
            
            Console.WriteLine("10) Change order status");
            Console.WriteLine("11) Log Out");
            Console.WriteLine("12) Exit");
        }



        



    }
}
