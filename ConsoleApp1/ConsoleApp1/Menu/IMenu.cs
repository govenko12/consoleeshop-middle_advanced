﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Menu
{
    interface IMenu
    {
        public string ChooseOptions();

        public bool IsActive { get; set; }
    }
}
