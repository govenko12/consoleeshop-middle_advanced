﻿using ConsoleApp1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConsoleEShop.Models;
namespace ConsoleApp1.servise
{
    public class CreateOrderService
    {
        public virtual List<OrderItem> Cart { get; set; }

        public Data _Data { get; set; }



        public CreateOrderService()
        {

        }

        public CreateOrderService(Data data, List<OrderItem> cart)
        {
            Cart = cart;
            _Data = data;
        }




        public string Create_New_Order(string name)
        {
            

            Foods prod = _Data.GetAllProducts().FirstOrDefault(prod => prod.Name == name);

            if (prod is null)
            {
                return "Sorry!! No such product in the store now.";
            }

            if (PresentInCart(prod.Name))
            {
                OrderItem productInCart = Cart.FirstOrDefault(item => item.product.Name == prod.Name);
                productInCart.Quantity++;
                return productInCart.ToString() + "Please take more )";


            }
            else
            {
                var newItem = new OrderItem(prod);
                Cart.Add(newItem);
                return newItem.ToString();
            }
        }


        private protected bool PresentInCart(string name)
        {
            return Cart.Any(item => item.product.Name == name);
        }

    }
}
