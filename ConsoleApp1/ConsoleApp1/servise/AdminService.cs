﻿using ConsoleApp1.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.servise
{
   public class AdminService
    {




        public Data _Data { get; set; }

        public AdminService(Data data)

        {
            _Data = data;
        }




        public string AddProduct(string name,int price)
        {
          


           
            if (price <= 0)
            {
                return "Invalid price!";
            }

            bool success = _Data.AddProduct(new Foods { Name = name, Price = price });
            if (success)
                return "Product added.";
            return "Name is invalid or already taken!";
        }

        public string ModifyProduct(int prodId,string newname,int newprice)
        {
           

            Foods prod = _Data.GetProductById(prodId);

            if (prod is null)
            {
                return "No product with such ID";
            }

            Foods newProd = new Foods();


            newProd.Name = newname;


            newProd.Price = newprice;


            if (newProd.Price <= 0)
            {
                return "Invalid price!";
            }

            bool success = _Data.UpdateProduct(prodId, newProd);
            if (success)
            {
                return "Product changed.";
            }
            return "Invaliv product data";
        }

    }
}
