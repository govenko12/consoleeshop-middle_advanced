﻿using ConsoleApp1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp1.servise
{
  public  class LoginService
    {

        public void Invoke (PersonalData user)
        {
            NotifyOfLogginIn?.Invoke(user);
        }

        public Data _Data { get; set; }

        public LoginService(Data data)
            
        {
            _Data = data;
        }


        public delegate void LogInHendler(PersonalData user);
        public event LogInHendler NotifyOfLogginIn;

        public string Log_in(string email, string password)
        {


            PersonalData user = _Data.GetAllUsers().FirstOrDefault(u => (u.Email == email) && (u.Password == password));

            if (user != null)
            {
                Invoke(user);
                return "WellCome back!";
            }
            else
            {
                return "Wrong email or password";
            }


        }




        public string Registration(string email,string password ,string confPass,string name)
        {
           

            if (!email.Contains("@"))
            {
                return "Email should contain '@' symbol.";
            }
            var registeredEmails = _Data.GetAllUsers().Select(u => u.Email);
            if (registeredEmails.Contains(email))
            {
                return "Email already registered.";
            }

            

            

            if (password != confPass)
            {
                return "Passwords don't match!";
            }

           


            PersonalData newUser = _Data.AddUser(email, password, name);

            Invoke(newUser);

            return "Registered";
        }







    }


}
