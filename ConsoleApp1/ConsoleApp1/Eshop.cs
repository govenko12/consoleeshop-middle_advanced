﻿using ConsoleApp1.Menu;
using ConsoleApp1.Models;
using ConsoleApp1.servise;
using ConsoleEShop.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Eshop
    {
        Data data;
        public IMenu menu;
        
        
        public Eshop()
        {
            data = new Data();
             menu = new GuestMenu(data, new servise.Food_Service(new Data()), new LoginService(data));
        }

        

       

        public void Start()
        {
            (menu as GuestMenu).LoginService.NotifyOfLogginIn += LogIn;

            while (menu.IsActive)
            {
                Console.Clear();

                Console.WriteLine(menu.ChooseOptions());
                Console.WriteLine("Press any key...");
                Console.ReadKey();


            }

        }


        public void LogIn(PersonalData user)
        {
            if (user.IsAdmin)
            {
                menu = new AdminMenu(new Data(),new CreateOrderService(new Data(), new List<OrderItem>()), user,new AdminService(new Data()));
                (menu as AdminMenu).NotifyOfLoggingOut += LogOut;
            }
            else
            {
                menu = new UserMenu(new Data(),new CreateOrderService(new Data(), new List<OrderItem>()),  user);
                (menu as UserMenu).NotifyOfLoggingOut += LogOut;
            }
        }

        public void LogOut()
        {
            menu = new GuestMenu(data,new servise.Food_Service(new Data()), new LoginService(data));
            (menu as GuestMenu).LoginService.NotifyOfLogginIn += LogIn;
        }


    }
}
