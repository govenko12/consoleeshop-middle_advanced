﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Models
{
  public  class PersonalData
   {
        public string Name { get; set; }

        public string Email { get; set; }
        public bool IsAdmin { get; set; } = false;
        public string Password { get; set; }

        public string Number { get; set; }

        private static int nextId = 0; //Temporary solution. ID must be assigned by DB
        public virtual int Id { get; set; }

        public PersonalData()
        {
            Id = nextId;
            nextId++;
        }
        public override string ToString()
        {
            string Role = IsAdmin ? "Admin" : "User";
            return $"Id: {Id}, Email: {Email}, password: {Password}, Role: {Role}";
        }

    }
}
