﻿using ConsoleApp1.Models;

namespace ConsoleEShop.Models
{
    public class OrderItem
    {
        public readonly Foods product;
        public int Quantity { get; set; } = 1;
        public OrderItem(Foods product)
        {
            this.product = product;
        }
        public override string ToString()
        {
            return $"Product: '{product.Name}', Quantity: {Quantity}";
        }
    }
}