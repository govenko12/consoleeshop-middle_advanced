﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Models
{
   public class Foods
    {
        public string Name { get; set; }

        public string Type { get; set; }

        public string Description { get; set; }

        public int Price { get; set; }




        public int Id { get; }
    
        private static int nextId = 0; //Temporary solution. Id must be assigned by DB
        public Foods()
        {
            Id = nextId;
            nextId++;
        }
    }
}
