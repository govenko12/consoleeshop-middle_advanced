﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Models
{
    interface IDO_order
    {

        string Create_New_Order();

        public string ConfirmOrder();
        public string ClearCart();
      
        public string ShowCart();
       
        public string CancelOrder();
        bool Get_status { get; set; }
    }
}
