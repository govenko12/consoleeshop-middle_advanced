﻿using ConsoleApp1.Models;
using ConsoleEShop.Models;
using System.Collections.Generic;

namespace ConsoleApp1
{
    public interface IRepository
    {
      
        List<Foods> GetAllProducts();
        Foods GetProductByName(string name);
        Foods GetProductById(int prodId);
        bool AddProduct(Foods product);
        bool UpdateProduct(int prodId, Foods product);

        void AddOrder(Order order);
        List<Order> GetOrdersByUserId(int userId);
        Order GetOrderById(int orderId);
        bool SetNewOrderStatus(int orderId, OrderStatus status);

        PersonalData AddUser(string email, string password);
        List<PersonalData> GetAllUsers();
        PersonalData GetUserById(int userId);
        PersonalData GetUserByEmail(string email);
        bool UpdateUser(PersonalData user);
    }
}
